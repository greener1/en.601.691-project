using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TargetLogic : MonoBehaviour
{
   
    public TextMeshPro debugText;                                                // for debugging info w/o actually debugging  
    public   TopArbiter topArbiter;
    private vMPLMovementArbiter arbiter = null;


    public GameObject cylinder;
   
    // Use this for initialization
    void Start()
    {
        arbiter = GameObject.Find("vMPLMovementArbiter").GetComponent<vMPLMovementArbiter>();
    }



    public void OnTriggerEnter(Collider other)
    {
        
        if (other.gameObject.name.Contains("Cylinder") ){
            other.gameObject.active = false;
        }
        if (other.gameObject.name.Contains("Cube") ){
            other.gameObject.active = false;
        }
        if (other.gameObject.name.Contains("Sphere") ){
            other.gameObject.active = false;
        }
        
    
    }


}
