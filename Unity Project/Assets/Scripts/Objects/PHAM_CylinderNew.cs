﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PHAM_CylinderNew : MonoBehaviour
{
    public int score;
    public bool successfulActivationCld;
    private GameObject LastHolderTouched;
    public Text scoreHUD;

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name.Contains("Holder") && other.gameObject.GetComponent<Holder>().isActivated())
        {
            //Failsafe incase cylinder glitches into the trigger space of another holder
            successfulActivationCld = true;
        }
        // Reset if object hits floor
        
        if (other.gameObject.name == "Floor")
        {
            //PHAM_ManagerPro.ColorHolder();
             GameObject.Find("Cylinder").transform.position = new Vector3(-1.25f,-0.26f,0.828f);
           //  GameObject.Find("Cylinder").transform.rotation = new Vector3(0f,0f,0f);

        }
    }
    public bool defSuccess()
    {
        successfulActivationCld = false;
        return successfulActivationCld;


    }

    public bool success()
    {
        return successfulActivationCld;
    }


}
