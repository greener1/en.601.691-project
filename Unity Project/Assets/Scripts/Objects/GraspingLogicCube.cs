using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GraspingLogicCube : MonoBehaviour
{
    public bool GraspingCube = false;
    public float norm_diff_cld;
    public int collision;
    public TextMeshPro debugText;                                                // for debugging info w/o actually debugging  
    public   TopArbiter topArbiter;

    public GameObject palm;
    private GameObject cylinder = null;
    private vMPLMovementArbiter arbiter = null;
    private Vector3 offset = new Vector3(.03f, -.045f, .05f);
    private const float GRASP_DIST_THRESHOLD = 1f;
    private const float GRASP_ANGLE_THRESHOLD = 2f;

    // Use this for initialization
    void Start()
    {
        GraspingCube = false;
        //palm = GameObject.Find("_rPalm"); // or Endpoint
        arbiter = GameObject.Find("vMPLMovementArbiter").GetComponent<vMPLMovementArbiter>();
    }



    public void OnTriggerEnter(Collider other)
    {


        if (topArbiter.i_EmgState != TopArbiter.EmgState.Open) {
            if (other.gameObject.name.Contains("Proximal") || other.gameObject.name.Contains("Distal") )
            {
                debugText.text = debugText.text + " " + other.gameObject.name;

                collision++;
            }

            if (collision >= 2 && topArbiter.i_GripState == TopArbiter.GripState.Tripod)
            {
                if (!GraspingCube) {
                    offset =gameObject.transform.position - palm.transform.position; 
                }
                GraspingCube = true;
            }

        }

    
    }

    

    // Update is called once per frame

    void Update()
    {
        float[] angles = arbiter.GetRightFingerAngles();
        
        if (GraspingCube){

            //GetComponent<Rigidbody>().mass = 0.01f;
            //GetComponent<Rigidbody>().useGravity = false;
            //GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
            
            
            gameObject.transform.position = palm.transform.position + offset;
            if (topArbiter.i_EmgState == TopArbiter.EmgState.Open){
                GraspingCube = false;
            }
        }
        if (!GraspingCube) {

        }
        //if (topArbiter.i_EmgState == TopArbiter.EmgState.Open) {
        if (topArbiter.i_EmgState != TopArbiter.EmgState.Rest && topArbiter.i_EmgState != TopArbiter.EmgState.Tripod) {

            collision = 0;
            debugText.text = "";
        }
        /* WHY IS THIS NOT WORKING 
        if (GraspingCylinder)
        {
           

            gameObject.transform.position = palm.transform.position
                                            - 0.45f * palm.transform.up
                                            - .3f * palm.transform.forward;
            gameObject.transform.up = palm.transform.right;
            norm_diff_cld = 0;

            debugText.text = "should be grasping now...";

            //if (angles[1] < 60.0 || angles[5] < 60.0 || angles[9] < 60.0 || angles[13] < 60.0 || angles[18] < 60.0 ) {
            //Grasping = false;
            // }
        } else
        {
            GetComponent<Rigidbody>().mass = 1;
            GetComponent<Rigidbody>().useGravity = true;
            GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;
            debugText.text = "no grasp";

        }

        WTF /*


        /*else
        {
            float angle_diff = Mathf.Min(Vector3.Distance(palm.transform.right, -1.0f * gameObject.transform.up),
                                          Vector3.Distance(palm.transform.right, gameObject.transform.up));
            Vector3 distance = palm.transform.position - gameObject.transform.position;

            //            Debug.Log(string.Format("Triggering...{0}, {1}", angle_diff, distance.magnitude));
            norm_diff_cld = distance.magnitude;
            //arbiter.GetMovementState() == vMPLMovementArbiter.MOVEMENT_STATE_CYLINDER_GRASP
            //&&
            

            if (angles[1] > 60.0 && angles[5] > 60.0 && angles[9] > 60.0 && angles[13] > 60.0 && angles[18] > 60.0 && angle_diff <= GRASP_ANGLE_THRESHOLD && norm_diff_cld <= GRASP_DIST_THRESHOLD)
            {
                GraspingCylinder = true;
                
            }
        }*/
        
        //if (PHAM_ManagerPro.whichObj()==1)
        //{
          //  if (!GraspingCylinder && GetComponent<PHAM_CylinderNew>().success())
          //  {
          //      collision = 0;
           //     PHAM_ManagerPro.nextTask();
            //    Debug.Log("ahhhh");
           // }


        //}
        /*
        else 
        {
            GetComponent<Rigidbody>().mass = 0.01f;
            GetComponent<Rigidbody>().useGravity = false;
            GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;


        }
        */
    }
}
