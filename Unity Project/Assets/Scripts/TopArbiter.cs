using Microsoft.MixedReality.Toolkit;
using Microsoft.MixedReality.Toolkit.Input;
using Microsoft.MixedReality.Toolkit.Utilities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TMPro;



public class TopArbiter : MonoBehaviour
{

    const int NUM_MPL_JOINT_ANGLES = 7;
    int dof = 0;
    float speed = 0.1f;

    private float yaw = 0.0f;
    private float pitch = 0.0f;

    public bool SharedControl = true;

	public enum EmgState : int {
        Invalid             = -1,
        Rest                = 0,
        Power     		    = 1,
        Open                = 2,
        Tripod              = 3,
        Key                 = 4,
        Pinch               = 5
    }

    public enum GripState : int {
        Invalid             = -1,
        Rest                = 0,
        Power               = 1,
        Open                = 2,
        Tripod              = 3,
        Key                 = 4,
        Pinch               = 5
    }

    public enum PlacementSurfaces
    {
        Horizontal,
        Vertical
    }

    // Private Variables

    private bool objIsGrabbed = false;                              // is the object grabbed?
    private bool placed = false;                                    // has the object been placed?
    private bool onlyTransitionToPlausibleDestinations = true;
    private IMixedRealityEyeGazeProvider EyeTrackingProvider => eyeTrackingProvider ?? (eyeTrackingProvider = CoreServices.InputSystem?.EyeGazeProvider);
    private IMixedRealityEyeGazeProvider eyeTrackingProvider = null;

    private Vector3 originalLoc;                                    // original object location before being moved


    // The values represent the maximum angle that the surface can be offset from the 'up' vector to be considered for horizontal placement.
    // For example, if a surface is slanted by 40 degrees targets may slid off and hence we may consider this an invalid offset angle.
    private float maxDiffAngleForHorizontalPlacem = 20;

    // The values represents the minimal angle that the surface must be offset from the 'up' vector to be considered for vertical placement.
    private float minDiffAngleForVerticalPlacem = 50;


    // Public Variables
    public EmgState i_EmgState = EmgState.Rest;                                  // EMG command
    public GripState i_GripState = GripState.Rest;                                // grip command

    public  GameObject targetObject = null;                                      // object being targed for interaction
    public Vector3 loc;                                                          // location selected for placing object
    public Vector3? plausibleLocation;                                           // checking plausible locations
    public TextMeshPro debugText;                                                // for debugging info w/o actually debugging  
    public TextMeshPro debugText3;                                                // for debugging info w/o actually debugging  

    // Private Variables
    private PlacementSurfaces PlacementSurface = PlacementSurfaces.Horizontal;   // is the object being placed on a horizontal or vertical surface
    private vMPLMovementArbiter arbiter = null;
    private float [] joint_angles = new float[NUM_MPL_JOINT_ANGLES];


    void Start()
    {
        arbiter = GameObject.Find("vMPLMovementArbiter").GetComponent<vMPLMovementArbiter>();

    } // function-- start

    void Update()
    {

        if (SharedControl) 
        {
            targetObject = EyeTrackingProvider?.GazeTarget; 
            
            if (i_GripState == GripState.Rest)
            {
                // grip type from most recent object looked at 
                if (targetObject.name.Contains("Cube")){
                    i_GripState = GripState.Tripod;
                    debugText.text = "cube";
                }

                if (targetObject.name.Contains("Cylinder")){
                    i_GripState = GripState.Power;
                    debugText.text = "cylinder";
                }    
                if (targetObject.name.Contains("Sphere")){
                    i_GripState = GripState.Pinch;
                    debugText.text = "sphere";
                }   
            }

        
            // state machince based off emg State
            switch(i_EmgState)
            {
                case EmgState.Rest:
                    i_GripState = GripState.Rest;
                    break;
                case EmgState.Power:

                    switch(i_GripState)
                    {
                        case GripState.Power: 
                            arbiter.SetMovementState(vMPLMovementArbiter.MovementState.PowerGrasp);
                            break;
                        case GripState.Tripod:
                            arbiter.SetMovementState(vMPLMovementArbiter.MovementState.TripodGrasp);
                            break;
                        case GripState.Pinch:
                            arbiter.SetMovementState(vMPLMovementArbiter.MovementState.PinchGrasp);
                            break; 
                        default:
                            arbiter.SetMovementState(vMPLMovementArbiter.MovementState.PowerGrasp);
                            break;
                    }
                

                    break;
                case EmgState.Open:
                    arbiter.SetMovementState(vMPLMovementArbiter.MovementState.HandOpen);
                    i_GripState = GripState.Open;
                    break;
                default:
                    break;
            }          
        }
        else
        {

            // state machince based off emg State
            switch(i_EmgState)
            {
                case EmgState.Rest:
                    i_GripState = GripState.Rest;
                    debugText.text = "rest";
                    break;
                case EmgState.Power:
                    i_GripState = GripState.Power;
                    arbiter.SetMovementState(vMPLMovementArbiter.MovementState.PowerGrasp);
                    debugText.text = "power";
                    break;
                case EmgState.Open:
                    i_GripState = GripState.Open;
                    arbiter.SetMovementState(vMPLMovementArbiter.MovementState.HandOpen);
                    debugText.text = "open";
                    break;
                case EmgState.Tripod:
                    i_GripState = GripState.Tripod;
                    arbiter.SetMovementState(vMPLMovementArbiter.MovementState.TripodGrasp);
                    debugText.text = "tripod";
                    break;
                case EmgState.Pinch:
                    i_GripState = GripState.Pinch;
                    arbiter.SetMovementState(vMPLMovementArbiter.MovementState.PinchGrasp);
                    debugText.text = "pinch";
                    break; 
                default:
                    break;
            }          
        }
    } // function - Update

	//---------------------------------------
    // FUNCTIONS
    //---------------------------------------

    public void SetEmgState(EmgState i_NewEmgState)
    {
        i_EmgState = i_NewEmgState;

    }//function - SetEmgState


    public EmgState GetEmgState()
    {
        return i_EmgState;

    }//function - GetEmgState

    private void DragObj_Start()
    {
        if (!objIsGrabbed)
        {
            targetObject = EyeTrackingProvider?.GazeTarget; 
            originalLoc =  targetObject.transform.position; 
            placed = false;      
        }
        

        if (targetObject != null)
        {
            objIsGrabbed = true;
        }

    } // function-- DragObj_Start

     private void Reset()
    {
            objIsGrabbed = false;
            placed = false;
            if (targetObject != null)
            {
            targetObject.transform.position = originalLoc;
            }
    } // function-- Reset

    private void PlaceObj()
    {
      //  loc = EyeTrackingProvider.HitPosition;
            
        if (!placed)
        {
            targetObject.transform.position = new Vector3(loc.x,loc.y,loc.z);
            placed = true;
        }
    } // function-- PlaceObj

    private void SetLoc()
    {
       if ((EyeTrackingProvider?.GazeTarget != null) &&
            (EyeTrackingProvider?.GazeTarget != targetObject))     // To prevent trying to place it on itself
        {
            plausibleLocation = null;

            //if (EyeTrackingProvider?.GazeTarget.GetComponent<SnapTo>() != null) // original line
            //if (EyeTrackingProvider?.GazeTarget != null)
            //{
            //    print("b");
            //    plausibleLocation = EyeTrackingProvider?.GazeTarget.transform.position;
            //} 
            // Determine the location to place the selected target at
            //else if (onlyTransitionToPlausibleDestinations)
            if (onlyTransitionToPlausibleDestinations)
            {
                if (IsDestinationPlausible())
                {
                    plausibleLocation = EyeTrackingProvider?.HitPosition;
                }
                else
                {
                    plausibleLocation = GetValidPlacemLocation(EyeTrackingProvider?.GazeTarget);
                }
            }
            else
            {
                plausibleLocation = EyeTrackingProvider?.HitPosition;
            }

        }
        loc = plausibleLocation?? new Vector3(0,0,0);

    } // function-- SetLoc

    // // <summary>
    // Check if the destination is plausible. For example, this means if the target is placeable
    /// on horizontal surfaces then only show a preview for (more or less) horizontal surfaces. 
    /// </summary>
    /// <returns>True if the target can be placed on this surface.</returns>
    private bool IsDestinationPlausible()
    {
        if (PlacementSurface == PlacementSurfaces.Horizontal)
        {
            float angle = Vector3.Angle(EyeTrackingProvider.HitNormal, Vector3.up);
            if (angle < maxDiffAngleForHorizontalPlacem) // If the angle is more than for example 20 degrees off from the up vector
            {
                return true;
            }
        }

        else if (PlacementSurface == PlacementSurfaces.Vertical)
        {
            float angle = Vector3.Angle(EyeTrackingProvider.HitNormal, gameObject.transform.up);
            if (angle > minDiffAngleForVerticalPlacem)
            {
                gameObject.transform.forward = -EyeTrackingProvider.HitNormal;
                return true;
            }
        }
        return false;
    } // function-- IsDestinationPlausible

    /// <summary>
    /// Retrieve a valid location for placing the target.
    /// </summary>
    private Vector3 GetValidPlacemLocation(GameObject hitobj)
    {
        // Determine position
        Vector3 tCenter = hitobj.transform.position;
        Vector3 tScale = hitobj.transform.lossyScale;
        Vector3 vUp = Vector3.up;

        Vector3 newPos = (tCenter + tScale.y / 2 * Vector3.up);
        return newPos;
    } // GetValiePlacemLocation

    void print_joint_angles()
    {
        string debug = "Joint Angles: (";
        for (int i=0; i<NUM_MPL_JOINT_ANGLES-1; i++)
        {
            debug += joint_angles[i].ToString() + ", ";
        }
        debug += joint_angles[NUM_MPL_JOINT_ANGLES - 1] + ")";
        Debug.Log(debug);
    } //print_joint_angles()





             
} // class TopArbiter

