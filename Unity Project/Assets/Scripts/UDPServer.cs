using UnityEngine;
using System;
using System.IO;
using System.Text;
using System.Linq;
using System.Collections.Generic;

#if !UNITY_EDITOR && UNITY_WSA
using Windows.Networking.Sockets;
using Windows.Storage.Streams;

#else
using System.Net;
using System.Net.Sockets;
using System.Threading;
#endif

public class UDPServer : MonoBehaviour
{
    private string objectName;
    static string port = "8051";
    private Queue<string> receivedUDPPacketQueue = new Queue<string>();

    private void Awake()
    {
        objectName = name;
    }

    public string GetLatestUDPPacket()
    {
        string message = "";
        while (receivedUDPPacketQueue.Count > 0)
        {
            message = receivedUDPPacketQueue.Dequeue();
        }
        return message;
    }

    /* UDP Server: Main Implementation */
#if !UNITY_EDITOR && UNITY_WSA

    DatagramSocket socket;
    async void Start(){
        //Debug.Log("Called Start Function");
        try
        {
            socket = new DatagramSocket();
            if(socket != null){Debug.Log("Socket created");}
            // The ConnectionReceived event is raised when connections are received.
            socket.MessageReceived += Socket_MessageReceived;
            // Start listening for incoming UDP connections on the specified port. 
            // You can specify any port that's not currently in use.
            await socket.BindServiceNameAsync(port);
            //Debug.Log(objectName + ": DatagramSocket setup done...");
        }
        catch (Exception e)
        {
            Debug.Log("Exception arised: " + e.Message);
            string webErrorMsg = SocketError.GetStatus(e.GetBaseException().HResult).ToString();
            Debug.Log("Web Error Status: " + webErrorMsg);
        }
    }

    async void Socket_MessageReceived(DatagramSocket sender, DatagramSocketMessageReceivedEventArgs args){
        Debug.Log("Socket Received from "+args.RemoteAddress);
        string request;
        using (DataReader dataReader = args.GetDataReader()){
            request = dataReader.ReadString(dataReader.UnconsumedBufferLength).Trim();
        }
        //Debug.Log("Request: " + request);
        receivedUDPPacketQueue.Enqueue(request);

        // Echo the request back as the response.
        using (Stream outputStream = (await sender.GetOutputStreamAsync(args.RemoteAddress, port)).AsStreamForWrite())
        {
            using (var streamWriter = new StreamWriter(outputStream))
            {
                await streamWriter.WriteLineAsync(request);
                await streamWriter.FlushAsync();
            }
        }


        sender.Dispose();


    } // end function-- Socket_MessageReceived


    void OnDestroy()
    {
        socket.Dispose();
    }
#endif
}
