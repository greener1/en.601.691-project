using UnityEngine;

using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;

#if UNITY_EDITOR
using System.Net.Sockets;
using System.Threading;
using System.Net;

#else
using System.Net;
using Windows.Networking.Sockets;
using Windows.Storage.Streams;
using System.Threading.Tasks;
using System.IO;
using Windows.Networking;
#endif


#if UNITY_EDITOR

public class BroadcastReceiver : MonoBehaviour
{
    //OnMessageReceived
    public delegate void AddOnMessageReceivedDelegate(string message, IPEndPoint remoteEndpoint);
    public event AddOnMessageReceivedDelegate MessageReceived;
    private void OnMessageReceivedEvent(string message, IPEndPoint remoteEndpoint)
    {
        if (MessageReceived != null)
            MessageReceived(message, remoteEndpoint);
    }

    private Thread _ReadThread;
    private UdpClient _Socket;


    int port = 8888; 
    void Start()
    {
        // create thread for reading UDP messages
        _ReadThread = new Thread(new ThreadStart(delegate
        {
            try
            {
                _Socket = new UdpClient(port);
               // Debug.LogFormat("Receiving on port {0}", port);
            }
            catch (Exception err)
            {
               // Debug.LogError(err.ToString());
                return;
            }
            while (true)
            {
                try
                {
                    // receive bytes
                    IPEndPoint anyIP = new IPEndPoint(IPAddress.Any, 0);
                    byte[] data = _Socket.Receive(ref anyIP);

                    // encode UTF8-coded bytes to text format
                    string message = Encoding.UTF8.GetString(data);
                    OnMessageReceivedEvent(message, anyIP);
                }
                catch (Exception err)
                {
                  //  Debug.LogError(err.ToString());
                }
            }
        }));
        _ReadThread.IsBackground = true;
        _ReadThread.Start();
    }

    public void Dispose()
    {
        if (_ReadThread.IsAlive)
        {
            _ReadThread.Abort();
        }
        if (_Socket != null)
        {
            _Socket.Close();
            _Socket = null;
        }
    }
}

#else

public class BroadcastReceiver : MonoBehaviour
{
    //OnMessageReceived
    public delegate void AddOnMessageReceivedDelegate(string message, IPEndPoint remoteEndpoint);
    public event AddOnMessageReceivedDelegate MessageReceived;
    private void OnMessageReceivedEvent(string message, IPEndPoint remoteEndpoint)
    {
        if (MessageReceived != null)
            MessageReceived(message, remoteEndpoint);
    }


    DatagramSocket _Socket = null;
    private int port = 8888; 

    string x;
    string y;
    string z; 
    string ax;
    string ay;
    string az;

    Vector3 pos;

    //private GameObject calibrator; 

    public async void Start()
    {

        string portStr = port.ToString();
        // start the client
         Debug.Log(string.Format("Attempting to start server"));

        try
        {
            _Socket = new DatagramSocket();
            _Socket.MessageReceived += _Socket_MessageReceived;

            await _Socket.BindServiceNameAsync(portStr);

            //await _Socket.BindEndpointAsync(null, portStr);

            //await _Socket.ConnectAsync(new HostName("255.255.255.255"), portStr.ToString());


            //HostName hostname = Windows.Networking.Connectivity.NetworkInformation.GetHostNames().FirstOrDefault();
            //var ep = new EndpointPair(hostname, portStr, new HostName("255.255.255.255"), portStr);
            //await _Client.ConnectAsync(ep)
;
            Debug.Log(string.Format("Receiving on {0}", portStr));

            await Task.Delay(3000);
            // send out a message, otherwise receiving does not work ?!
            var outputStream = await _Socket.GetOutputStreamAsync(new HostName("10.194.56.156"), portStr);
            DataWriter writer = new DataWriter(outputStream);
            writer.WriteString("Initial Message");
            await writer.StoreAsync();
        }
        catch (Exception ex)
        {
            _Socket.Dispose();
            _Socket = null;
            Debug.LogError(ex.ToString());
            Debug.Log(string.Format("error starting socket"));
            Debug.LogError(Windows.Networking.Sockets.SocketError.GetStatus(ex.HResult).ToString());
        }

       // calibrator = GameObject.Find("MPLTransRadialCalibrator");

    }

    // Update is called once per frame
    void Update(){
        //Debug.Log(pos);
        //Debug.Log(calibrator.transform.position);
        //calibrator.transform.position = pos;
        //Debug.Log(calibrator.transform.position);


    }

    private async void _Socket_MessageReceived(DatagramSocket sender, DatagramSocketMessageReceivedEventArgs args)
    {
        Debug.Log(string.Format("Got Something!"));

        try
        {
            Stream streamIn = args.GetDataStream().AsStreamForRead();
            StreamReader reader = new StreamReader(streamIn, Encoding.UTF8);

            string message = await reader.ReadLineAsync();
            IPEndPoint remoteEndpoint = new IPEndPoint(IPAddress.Parse(args.RemoteAddress.RawName), Convert.ToInt32(args.RemotePort));
            OnMessageReceivedEvent(message, remoteEndpoint);

            ParseMessage(message);
        }
        catch (Exception ex)
        {
            Debug.LogError(ex.ToString());
        }
    }

    public void Dispose()
    {
        if (_Socket != null)
        {
            _Socket.Dispose();
            _Socket = null;
        }
    }

    private void ParseMessage(string message)
    {

        int start = 0;
        int end = message.Length;
        int count;
        int at = 0;
        int num_found = 0; 


        float[] values = new float[6];

        Debug.Log(string.Format(message));

        // find indcies of decimal points
        while((start <= end) && (at > -1))
        {
            // start+count must be a position within -str-.
            count = end - start;
            at = message.IndexOf(".", start, count);
            if (at == -1) break;
            values[num_found] = float.Parse(message.Substring(start, at+4-start));

            start = at+4;
            num_found = num_found +1;

        }

        pos.Set(values[0], values[1], values[2]);
  

    }
}

#endif