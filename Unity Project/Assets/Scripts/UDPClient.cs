using UnityEngine;
using System;
using System.IO;
using System.Text;
using System.Linq;
using System.Collections.Generic;

#if !UNITY_EDITOR && UNITY_METRO
using Windows.Networking;
using Windows.Networking.Sockets;
using Windows.Storage.Streams;
#else
using System.Net;
using System.Net.Sockets;
using System.Threading;
#endif

public class UDPClient : MonoBehaviour
{
#if !UNITY_EDITOR && UNITY_METRO

    DatagramSocket socket;
    HostName hostName;
    string port = "8888";
    async void Start(){
        //Debug.Log("Called Start Function");
        try
        {
            socket = new DatagramSocket();
            if(socket != null){Debug.Log("Socket created");}
            
            hostName = new Windows.Networking.HostName("192.168.50.124");  // IP address of Hand
            Debug.Log("Client Trying to bind..");

            await socket.BindServiceNameAsync(port);
            Debug.Log("Client Bound to port number 8888");
    
            
        }
        catch (Exception e)
        {
            Debug.Log("Exception arised: " + e.Message);
            string webErrorMsg = SocketError.GetStatus(e.GetBaseException().HResult).ToString();
            Debug.Log("Web Error Status: " + webErrorMsg);
        }
    }

    void Update()
    {
        sendToHand("test123");
    }
    public async void sendToHand(string request){
        // Send a request to the echo server.
        using (var serverDatagramSocket = new DatagramSocket())
        {
            using (Stream outputStream = (await serverDatagramSocket.GetOutputStreamAsync(hostName, port)).AsStreamForWrite())
            {
                using (var streamWriter = new StreamWriter(outputStream))
                {
                    await streamWriter.WriteLineAsync(request);
                    await streamWriter.FlushAsync();
                }
            }
        }
        Debug.Log("Sent Message to Server");
    
    }

#endif
}

