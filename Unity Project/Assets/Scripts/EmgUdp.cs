using UnityEngine;
using TMPro;

using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;

#if UNITY_EDITOR
using System.Net.Sockets;
using System.Threading;
using System.Net;

#else 
using System.Net;
using Windows.Networking.Sockets;
using Windows.Storage.Streams;
using System.Threading.Tasks;
using System.IO;
using Windows.Networking;
#endif



public class EmgUdp : MonoBehaviour
{

    //debug tools 
    public TextMeshPro txt;
    private string debugText;

    // access to state machine
    private TopArbiter arbiter = null;


#if !UNITY_EDITOR
    // socket information
    DatagramSocket _Socket = null;
    private int port = 8888; 


 public async void Start()
    {
        arbiter = GameObject.Find("TopArbiter").GetComponent<TopArbiter>();


        string portStr = port.ToString();
        // start the client
         Debug.Log(string.Format("Attempting to start server"));

        try
        {
            _Socket = new DatagramSocket();
            _Socket.MessageReceived += _Socket_MessageReceived;

            await _Socket.BindServiceNameAsync(portStr);

            //await _Socket.BindEndpointAsync(null, portStr);

            //await _Socket.ConnectAsync(new HostName("255.255.255.255"), portStr.ToString());


            //HostName hostname = Windows.Networking.Connectivity.NetworkInformation.GetHostNames().FirstOrDefault();
            //var ep = new EndpointPair(hostname, portStr, new HostName("255.255.255.255"), portStr);
            //await _Client.ConnectAsync(ep)

            Debug.Log(string.Format("Receiving on {0}", portStr));
 
            await Task.Delay(3000);
            // send out a message, otherwise receiving does not work ?!
            var outputStream = await _Socket.GetOutputStreamAsync(new HostName("10.194.56.156"), portStr);  // desktop in lab 
            //var outputStream = await _Socket.GetOutputStreamAsync(new HostName("192.168.50.124"), portStr); // laptop at home

            DataWriter writer = new DataWriter(outputStream);
            writer.WriteString("Initial Message");
            await writer.StoreAsync();
        }
        catch (Exception ex)
        {
            _Socket.Dispose();
            _Socket = null;
            Debug.LogError(ex.ToString());
            Debug.Log(string.Format("error starting socket"));
            Debug.LogError(Windows.Networking.Sockets.SocketError.GetStatus(ex.HResult).ToString());
        }

    } // start()


     // Update is called once per frame
    void Update(){

        txt.text = debugText;
        
    }

    private async void _Socket_MessageReceived(DatagramSocket sender, DatagramSocketMessageReceivedEventArgs args)
    {
        Debug.Log(string.Format("Got Something!"));

        try
        {
            Stream streamIn = args.GetDataStream().AsStreamForRead();
            StreamReader reader = new StreamReader(streamIn, Encoding.UTF8);

            string message = await reader.ReadLineAsync();
            IPEndPoint remoteEndpoint = new IPEndPoint(IPAddress.Parse(args.RemoteAddress.RawName), Convert.ToInt32(args.RemotePort));

            ParseMessage(message);
        }
        catch (Exception ex)
        {
            Debug.LogError(ex.ToString());
        }
    } //_Socket_MessageReceived()

        public void Dispose()
    {
        if (_Socket != null)
        {
            _Socket.Dispose();
            _Socket = null;
        }
    } // Dispose()

    private void ParseMessage(string message)
    {
        debugText = message;

        if (message == "REST") {
            arbiter.i_EmgState =  TopArbiter.EmgState.Rest;
        }
        if (message == "POWER") {
            arbiter.i_EmgState =  TopArbiter.EmgState.Power;
        }
        if (message == "OPEN") {
            arbiter.i_EmgState =  TopArbiter.EmgState.Open;
        }
        if (message == "TRIPOD") {
            arbiter.i_EmgState =  TopArbiter.EmgState.Tripod;
        }
        if (message == "KEY") {
            arbiter.i_EmgState =  TopArbiter.EmgState.Key;
        }
        if (message == "PINCH") {
            arbiter.i_EmgState =  TopArbiter.EmgState.Pinch;
        }
    } //ParseMessage




#endif


} // EmgUdp class

