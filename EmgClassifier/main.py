import sys, time, pickle, socket
import numpy as np
np.set_printoptions( precision = 3, threshold=np.inf)

import matplotlib.pyplot as plt
from sklearn.preprocessing import RobustScaler


#local imports 
from SenseController import SenseController
from MavFilter import MavFilter
from Proportional_Controller import Proportional_Controller
from NaiveBayesGaussian import NaiveBayesGaussian
from ModifiedEASRC import ModifiedEASRC
########## Constants #######################
COLLECT_DATA = 1       # whether to collct data or load from .p
TRAIN_TIME = 10        # amount of train data to collect per class
TEST_TIME = 30
WINDOW_SIZE = 50
EMG_WINDOW_STEP = 10    # space between overlaping windows
STABLE_WINDOW = 10      # number of necessary same measurements to leave rest 
#EMG_SCALING_FACTOR = 10000.0
EMG_SCALING_FACTOR = 1.0


UDP_IP = "127.0.0.1"
UDP_PORT = 5005

sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP

ONSET_THRESHOLD = 200

mav = MavFilter() 
########## Init Sensors #####################

#controller = Proportional_Controller()

if COLLECT_DATA:
    print('Initializing Sense Controller....')
    sense  = SenseController(mac = 'ec:fe:7e:1b:8e:bd', num_electrodes = 8, srate = 1000, mode = 'emg')
    sense.run()
    print('done')

######### Collect data #######################
if COLLECT_DATA:
    print('Rest Position') 
    time.sleep(1)
    print('Collecting rest data...')

    rest_data = []
    t0 = time.time()
    while time.time() - t0 < TRAIN_TIME:
        emg = sense.state
        if emg is not None:
            rest_data.append(emg)

    print('done')
    print('Saving Rest data...')
    pickle.dump( np.array(rest_data), open( "rest.p", "wb" ) )
    print('done')
    time.sleep(1)

    print('Close Position') 
    time.sleep(1)
    print('Collecting close data...')

    close_data = []
    t0 = time.time()
    while time.time() - t0 < TRAIN_TIME:
        emg = sense.state
        if emg is not None:
            close_data.append(emg)

    print('done')
    print('Saving Rest data...')
    pickle.dump( np.array(close_data), open( "close.p", "wb" ) )
    print('done')
    time.sleep(1)

    print('Open Position') 
    time.sleep(1)
    print('Collecting open data...')

    open_data = []
    t0 = time.time()
    while time.time() - t0 < TRAIN_TIME:
        emg = sense.state
        if emg is not None:
            open_data.append(emg)

    print('done')
    print('Saving open data...')
    pickle.dump( np.array(open_data), open( "open.p", "wb" ) )
    print('done')
    time.sleep(1)

#################### Import and preprocess Data  ############################
print('Preprocessing data...')
rest_raw_data = pickle.load( open( "rest.p", "rb" ) )
open_raw_data = pickle.load( open( "open.p", "rb" ) )
close_raw_data = pickle.load( open( "close.p", "rb" ) )


min_length = min([len(rest_raw_data), len(open_raw_data), len(close_raw_data)])

rest_data =  np.zeros([int(min_length/50),8])
open_data =  np.zeros([int(min_length/50),8])
close_data = np.zeros([int(min_length/50),8])

for i in range(int(min_length/50)):
    rest_data[i,:] =  mav.filter(abs(rest_raw_data[50*i:50*i+50, :])) 
    open_data[i,:] =  mav.filter(abs(open_raw_data[50*i:50*i+50, :])) 
    close_data[i,:] = mav.filter(abs(close_raw_data[50*i:50*i+50, :])) 

print('done')
'''
fig, axs = plt.subplots(8)

for i in range(8):

    axs[i].plot(rest_data[:,i])
    axs[i].plot(close_data[:,i])
    axs[i].plot(open_data[:,i])

plt.show()
'''
#################### Train Controller  ############################
print('training Controller')
prop = Proportional_Controller(onset_threshold = ONSET_THRESHOLD)

X = np.concatenate((rest_data,open_data, close_data), axis=0)
L = np.concatenate((0*np.ones([len(rest_data),1]),1*np.ones([len(open_data),1]), 2*np.ones([len(close_data),1])), axis=0 )

prop._init_limits(X,L)

scaler = RobustScaler()
scaler.fit( X )
Xtrain = scaler.transform( X )
ytrain = np.squeeze(L)

#mdl = ModifiedEASRC()
#mdl.train( Xtrain, ytrain )
mdl = NaiveBayesGaussian(Xtrain, ytrain)

########################## Run Controlller ######################
num_electrodes = 8
emg_window = []
output_predictions = []
unfiltered = []
cont = True
i = 0

old_pred = 0
pred_count = 0
last_stable_pred = 0
t0 = time.time()
while cont:
    if (COLLECT_DATA):
        if time.time() - t0 > TEST_TIME:
            cont = False
        data = sense.state
        if data is not None:
            emg_window.append( data[:num_electrodes].copy() )
            if len( emg_window ) == WINDOW_SIZE:
                t = time.time()

                # preprocessing
                win = np.vstack( emg_window ) / EMG_SCALING_FACTOR
                emg_window = emg_window[EMG_WINDOW_STEP:] 
                feat = mav.filter( win )
                feat_scaled = scaler.transform( feat.reshape( 1, -1 ) )
        
                if np.mean(feat_scaled[:8]) > 0.05:
                    pred = int( mdl.predict( feat_scaled )[0] + 1 )    # classify feature vectore
                else:
                    pred = 0


                #output filtering 
                if(pred == old_pred):
                    pred_count +=1
                else:
                    pred_count = 0
            
                if pred_count > STABLE_WINDOW:
                    output_predictions.append( pred )
                    unfiltered.append( pred )
                    last_stable_pred = pred
                else:
                    output_predictions.append( last_stable_pred )
                    unfiltered.append( pred )
                old_pred = pred

            
    else:
        i = i+1
        cont = i < len(Xtrain)-1
        feat_scaled = Xtrain[i,:].reshape(1, -1)  
    # classify
            #pred = (mdl.predict( feat_scaled ) )
            #pred = int( mdl.predict( feat_scaled )[0] + 1 )    # classify feature vectore
    #thresholding
        if np.mean(feat_scaled[:8]) > 0.05:
            pred = int( mdl.predict( feat_scaled )[0] + 1 )    # classify feature vectore
        else:
            pred = 0

            # velocity
            #if pred == 0: #rest
            #    prop_vel = 0
            #else:
            #    prop_vel = prop.Proportional( feat, pred-1, PROP_SMOOTH ) if prop_enable else 1.0  # proportional control

        #output filtering 
        if(pred == old_pred):
            pred_count +=1
        else:
            pred_count = 0
            
        if pred_count > STABLE_WINDOW:
            output_predictions.append( pred )
            unfiltered.append( pred )
            last_stable_pred = pred
            sock.sendto(pred, (UDP_IP, UDP_PORT))

        else:
            output_predictions.append( last_stable_pred )
            unfiltered.append( pred )
        old_pred = pred

########################## Plots  ######################
'''
fig, axs = plt.subplots(8)

axs[0].plot(ytrain)
ax = 1
for i in [0,2,4,5,6,7]:
    axs[ax].plot(Xtrain[:,i])
    ax +=1
plt.show()
'''
plt.plot(output_predictions,color='b')
#plt.plot(unfiltered,color='r')
plt.show()

#################### Close Connections ############################
print('Closing Connections...')
if COLLECT_DATA:
    sense.stop()
print('Connections closed.')
