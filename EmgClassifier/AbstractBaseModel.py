# define abstract base class
import abc
class AbstractBaseModel(object, metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def train(self, X, y):
        raise NotImplementedError("Users must define 'train' to use this base class")
       
    @abc.abstractmethod
    def predict(self, X):
        raise NotImplementedError("Users must define 'predict' to use this base class")

