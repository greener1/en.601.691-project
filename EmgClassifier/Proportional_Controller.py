import sys, time,os
import numpy as np
from collections import deque

import scipy.io as sio
import scipy.stats as stats
import scipy.special as scsp


class Proportional_Controller:
    def __init__ (self, classes = ['open', 'close'], onset_threshold = 0):
        self._classes = classes
        self._n_classes = len(classes)
        self._range = np.zeros((2, self._n_classes))
        self._rms = deque()

        self.auto_percentile_low = 10
        self.auto_percentile_high = 50

        self._onset_threshold = onset_threshold

        self.prop_limits = { 'open' :       [ 0, 0 ],
                             'close' :      [ 0, 0 ],
                            }

        self.prop_percentile = { 'open' :       [ 0, 0 ],
                                 'close' :      [ 0, 0 ],
                            }

    def _init_limits(self, X, L):
        for i in range(self._n_classes):
            class_idx = np.where(L == i+1)
            class_temp = X[ 0:8, class_idx[-1] ]
            class_means = np.mean(class_temp,0)

            z_idx = np.where(class_means > self._onset_threshold)[0]

            z_scores = stats.zscore(class_means[z_idx])

            p_values = np.zeros(class_means.size)
            for j in range(z_idx.size):
                p_values[z_idx[j]] = 0.5 * (1 + scsp.erf(z_scores[j] / np.sqrt(2)))


            idx = (np.abs(p_values - (self.auto_percentile_low/100) )).argmin()

            self._range[0][i] = class_means[idx]
            self.prop_limits[self._classes[i]][0] = class_means[idx]

            idx = (np.abs(p_values - (self.auto_percentile_high/100) )).argmin()

            self._range[1][i] = class_means[idx]
            self.prop_limits[self._classes[i]][0] =  class_means[idx]

            self.prop_percentile[self._classes[i]][0] = self.auto_percentile_low/100
            self.prop_percentile[self._classes[i]][1] = self.auto_percentile_high/100


