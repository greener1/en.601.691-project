import struct
import socket

class VirtualModularProstheticLimb:
    UDP_SERVER_ADDR = ( '127.0.0.1', 10000 )
    UDP_JOINT_START_BYTE = b'\x6a'

    NUM_JOINT_ANGLES = 27
    MOVEMENT_CLASS_IDX = { 'rest' : 0, 'open' : 15, 
                           'power' : 16, 'tripod' : 17, 'key' : 18, 'pinch' : 19, 'index' : 20, 
                           'pronate' : 9, 'supinate' : 10 }

    def __init__( self, ip = '127.0.0.1', port = 9027 ):
        """
        Constructor

        Parameters
        ----------
        ip : str
            The remote IP of the Hololens
        port : int
            The remote port that the Hololens is listening on
        """
        # create UDP socket
        self._udp_tx = ( ip, port )
        self._sock = socket.socket( socket.AF_INET, socket.SOCK_DGRAM )
        self._sock.bind( VirtualModularProstheticLimb.UDP_SERVER_ADDR )
        self._sock.setblocking( False )

        self._last_angles = [ 0.0 for _ in range( VirtualModularProstheticLimb.NUM_JOINT_ANGLES ) ]
        self._last_class = None

    def publish( self, move = None, joint_angles = None ):
        """
        Parameters
        ----------
        move : str or None
            A movement class id
        joint_angles : iterable of floats (n_joints,)
            Joint angles to send to each joint
        """
        if joint_angles is not None: angles = joint_angles
        else: angles = self._last_angles

        if move is not None:
            try:
                move_idx = VirtualModularProstheticLimb.MOVEMENT_CLASS_IDX[move]
                self._last_class = move
            except KeyError:
                move_idx = -1
        else:
            move_idx = -1

        # send data over UDP
        if move != self._last_class or joint_angles != self._last_angles:
            fmt = '<ci' + VirtualModularProstheticLimb.NUM_JOINT_ANGLES * 'f'
            data = struct.pack( fmt, VirtualModularProstheticLimb.UDP_JOINT_START_BYTE, move_idx, *angles )
            self._sock.sendto( data, self._udp_tx )

        # store this configuration
        self._last_angles = angles

if __name__ == '__main__':
    import sys
    import inspect
    import argparse

    import time
    import numpy as np

    # helper function for booleans
    def str2bool( v ):
        if v.lower() in [ 'yes', 'true', 't', 'y', '1' ]: return True
        elif v.lower() in [ 'no', 'false', 'n', 'f', '0' ]: return False
        else: raise argparse.ArgumentTypeError( 'Boolean value expected!' )

    # parse commandline entries
    class_init = inspect.getargspec( VirtualModularProstheticLimb.__init__ )
    arglist = class_init.args[1:]   # first item is always self
    defaults = class_init.defaults
    parser = argparse.ArgumentParser()
    for arg in range( 0, len( arglist ) ):
        try: tgt_type = type( defaults[ arg ][ 0 ] )
        except: tgt_type = type( defaults[ arg ] )
        if tgt_type is bool:
            parser.add_argument( '--' + arglist[ arg ], 
                             type = str2bool, nargs = '?',
                             action = 'store', dest = arglist[ arg ],
                             default = defaults[ arg ] )
        else:
            parser.add_argument( '--' + arglist[ arg ], 
                                type = tgt_type, nargs = '+',
                                action = 'store', dest = arglist[ arg ],
                                default = defaults[ arg ] )
    args = parser.parse_args()
    for arg in range( 0, len( arglist ) ):
        attr = getattr( args, arglist[ arg ] )
        if isinstance( attr, list ) and not isinstance( defaults[ arg ], list ):
            setattr( args, arglist[ arg ], attr[ 0 ]  )

    # create interface
    vmpl = VirtualModularProstheticLimb( ip = args.ip, port = args.port )
    moves = [ 'rest', 'open', 'power', 'tripod', 'key', 'pinch', 'index' ,'pronate', 'supinate' ]

    print( '------ Movement Commands -------' )
    print( '| 00  -----  REST              |' )
    print( '| 01  -----  HAND OPEN         |' )
    print( '| 02  -----  POWER GRASP       |' )
    print( '| 03  -----  TRIPOD GRASP      |' )
    print( '| 04  -----  KEY GRASP         |' )
    print( '| 05  -----  PINCH GRASP       |' )
    print( '| 06  -----  INDEX POINT       |' )
    print( '| 07  -----  WRIST PRONATE     |' )
    print( '| 08  -----  WRIST SUPINATE    |' )  
    print( '--------------------------------' )
    print( '| Press [Q] to quit!           |' )
    print( '--------------------------------' )

    done = False  
    while not done:
        cmd = input( 'Command: ' )
        if cmd.lower() == 'q':
            done = True
        else:
            try:
                idx = int( cmd )
                if idx in range( 0, len( moves ) ):
                    vmpl.publish( move = moves[ idx ] )
            except ValueError:
                pass
    print( 'Bye-bye!' )


    # # create data
    # t = np.linspace( 0, 2 * np.pi, 20 )
    # angles = 180.0 * np.abs( np.vstack( [ np.sin( t ) for _ in range( VirtualModularProstheticLimb.NUM_JOINT_ANGLES ) ] ).T )

    # # send data over
    # for i in range( angles.shape[0] ):
    #     print( '(%d) Sending Angles:' % (i+1), angles[i,0] )
    #     holo.publish( move = None, joint_angles = angles[i,:] )
    #     time.sleep( 1 )