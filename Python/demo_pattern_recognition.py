import sys
import inspect
import argparse

import time
import random

import numpy as np

import matplotlib
matplotlib.use( 'QT5Agg')

import matplotlib.pyplot as plt
import matplotlib.image as mpimg

from scipy.io import loadmat

from MyoArmband import MyoArmband
from TimeDomainFilter import TimeDomainFilter
from LinearDiscriminantAnalysis import LinearDiscriminantAnalysis

from VirtualModularProstheticLimb import VirtualModularProstheticLimb

CLASSES = [ 'rest', 'power', 'open', 'pronate', 'supinate', 'tripod', 'key', 'pinch' ]

if __name__ == '__main__':

    # parse commandline entries
    parser = argparse.ArgumentParser()

    parser.add_argument( '--name', type = str, nargs = '+', action = 'store', dest = 'name', default = 'MyoArmband' )
    parser.add_argument( '--mac', type = str, nargs = '+', action = 'store', dest = 'mac', default = 'EB:33:40:96:CE:A5' )
    parser.add_argument( '--emg_window_size', type = int, nargs = '+', action = 'store', dest = 'emg_window_size', default = 50 )
    parser.add_argument( '--emg_window_step', type = int, nargs = '+', action = 'store', dest = 'emg_window_step', default = 10 )
    parser.add_argument( '--training_file', type = str, nargs = '+', action = 'store', dest = 'training_file', default = 'train.mat' )

    args = parser.parse_args()

    # command-line parameters
    name = args.name[0] if type( args.name ) is list else args.name
    mac = args.mac[0] if type( args.mac ) is list else args.mac
    emg_window_size = args.emg_window_size[0] if type( args.emg_window_size ) is list else args.emg_window_size
    emg_window_step = args.emg_window_step[0] if type( args.emg_window_step ) is list else args.emg_window_step
    training_file = args.training_file[0] if type( args.training_file ) is list else args.training_file

    # download training data
    print( 'Importing training data...', end = '', flush = True)
    training_data = loadmat( training_file )['train'][0]
    print( 'Done!' )

    # create feature extracting filters
    print( 'Creating EMG feature filters...', end = '', flush = True )
    td5 = TimeDomainFilter()
    print( 'Done!' )

    # compute training features and labels
    print( 'Computing features...', end = '', flush = True)
    num_trials = len( training_data )
    X = []
    y = []
    for i in range( len( CLASSES ) ):
        class_data = []
        for j in range( num_trials ):
            raw_data = training_data[ j ][ CLASSES[ i ] ][0][0]
            num_samples = raw_data.shape[0]
            idx = 0
            while idx + emg_window_size < num_samples:
                window = raw_data[idx:(idx+emg_window_size),:]
                time_domain = td5.filter( window ).flatten()
                class_data.append( np.hstack( time_domain ) )
                idx += emg_window_step
        X.append( np.vstack( class_data ) )
        y.append( i * np.ones( ( X[-1].shape[0], ) ) )
    X = np.vstack( X )
    y = np.hstack( y )
    print( 'Done!' )

    # train classifier
    print( 'Training LDA classifier...', end = '', flush = True )
    mdl = LinearDiscriminantAnalysis( X, y )
    print( 'Done!' )

    # create Myo interface
    print( 'Creating device interface...', end = '', flush = True )
    myo = MyoArmband( name = name, mac = mac )
    print( 'Done!' )

    # create virtual Modular Prosthetic Limb interface
    print( 'Creating vMPL interface...', end = '', flush = True )
    vmpl = VirtualModularProstheticLimb()
    print( 'Done!' )

    print( 'Starting data streaming...', end = '\n', flush = True )
    myo.run()
    try:
        emg_window = []
        while True:
            data = myo.state
            if data is not None:
                emg_window.append( data[:8].copy() )
                if len( emg_window ) == emg_window_size:
                    # print( 'here..' )
                    win = np.vstack( emg_window )
                    feat = td5.filter( win )
                    
                    pred = int( mdl.predict( feat.reshape( 1, -1 ) )[0] )
                    vmpl.publish( move = CLASSES[pred] )
                    print( 'CLASSIFIER OUTPUT: %s' % ( CLASSES[pred] ).upper() )

                    emg_window = emg_window[emg_window_step:]
    finally:
        myo.stop()
        myo.close()

    print( 'Done!' )